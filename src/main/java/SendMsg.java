import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.WaitOptions;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import utils.*;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;

import static io.appium.java_client.touch.offset.PointOption.point;

public class SendMsg {
    WebDriver driver;
    String excelPath = System.getProperty("user.dir") +"\\excel_file\\Input.xlsx";
    int rowNo = 3;
    AppiumDriverLocalService appiumService;
    String appiumServiceUrl;

    By fab_write_sms = By.id("com.enflick.android.TextNow:id/fabNewConversation");
    //By fab_write_sms = By.id("com.enflick.android.TextNow:id/fab");
    By input_field_number = By.id("com.enflick.android.TextNow:id/to_view");
    By input_field_text = By.id("com.enflick.android.TextNow:id/edit_text_out");
    By btn_send = By.id("com.enflick.android.TextNow:id/button_send");
    By btn_back = By.xpath("//android.widget.ImageButton[@content-desc = 'Navigate up']");
    By btn_cam = By.id("com.enflick.android.TextNow:id/image_button");
    By search_result = By.id("com.enflick.android.TextNow:id/contact_avatar");
    By msg_body = By.id("com.enflick.android.TextNow:id/message_text");

    public SendMsg() throws IOException {
    }

    @BeforeTest
    public void runAppiumServer() throws InterruptedException, IOException {
        System.out.println("&& Welcome to the send Msg BOT &&");
        //System.out.println("Starting the server");
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();
        appiumServiceUrl = appiumService.getUrl().toString();
        //System.out.println("Address: "+ appiumServiceUrl);
        appiumService.clearOutPutStreams();

        Thread.sleep(2000);
        Runtime.getRuntime().exec("adb connect localhost:21503");
        //System.out.println("MEmu connected");
        System.out.println("@@ The BOT is ready to go @@");

        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "android");
        dc.setCapability("deviceName", "OnePlusOne");
        dc.setCapability("noReset", "true");
        dc.setCapability("autoGrantPermissions", "true");
        dc.setCapability("udid", "localhost:21503");
        dc.setCapability("appPackage", "com.enflick.android.TextNow");
        dc.setCapability("appActivity", "authorization.ui.AuthorizationActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);
    }

    int numberOfContacts = (GetUsedRowCount.getCountUsedRow(excelPath, "0")) - 2;
    private Object[][] data = new Object[0][0];
    @DataProvider(name="NumberProvider")
    public Object[][] getNumber() {
        data = new Object[numberOfContacts][1];
        return data;
    }

    //For 1st Msg
    @Test(dataProvider="NumberProvider", priority = 1)
    public void sendMsgOne(String args) throws Exception {
        rowNo += 1;

        waitForVisibilityOf(search_result);

        String phone_number = ReadExcel.readData(excelPath, "0", rowNo, "1");
        String first_msg_status = ReadExcel.readData(excelPath, "0", rowNo, "4");

        if (first_msg_status.equals("Sent")){
            System.out.println("==> 1st message already sent to "+phone_number);
        }else {
            waitForVisibilityOf(fab_write_sms);
            driver.findElement(fab_write_sms).click();

            waitForClickAbilityOf(input_field_number);
            driver.findElement(input_field_number).click();
            driver.findElement(input_field_number).sendKeys(phone_number);

            String name = ReadExcel.readData(excelPath, "0", rowNo, "2");
            String first_msg = ReadExcel.readData(excelPath, "0", rowNo, "3");
            String add_first_msg = "Hi is the "+name+" "+first_msg;
            waitForClickAbilityOf(input_field_text);
            driver.findElement(input_field_text).click();
            driver.findElement(input_field_text).sendKeys(add_first_msg);

            waitForClickAbilityOf(btn_send);
            driver.findElement(btn_send).click();
            waitForVisibilityOf(btn_cam);
            System.out.println("==> 1st message sent to "+phone_number);

            WriteExcel.writeData(excelPath, 0, rowNo, 4, "Sent");
            WriteExcel.writeData(excelPath, 0, rowNo, 13, add_first_msg);

            waitForClickAbilityOf(btn_back);
            driver.findElement(btn_back).click();
            Thread.sleep(1000);
            if (driver.findElements(btn_back).size() > 0){
                driver.findElement(btn_back).click();
            }
        }
        System.out.println("*******************************************************");
    }

    @Test(priority = 2)
    public void showSep(){
        System.out.println("==================== 1st message part DONE ====================");
        System.out.println("==================== 2nd message part INIT ====================");
    }

    //For 2nd Msg
    int rowNo2 = 3;
    @Test(dataProvider="NumberProvider", priority = 3, enabled = true)
    public void sendMsgTwo(String args) throws Exception {
        rowNo2 += 1;

        waitForVisibilityOf(search_result);

        String phone_number = ReadExcel.readData(excelPath, "0", rowNo2, "1");
        String second_msg_status = ReadExcel.readData(excelPath, "0", rowNo2, "6");

        if (second_msg_status.equals("Sent")){
            System.out.println("==> Got reply for 1st message");
            System.out.println("==> 2nd Msg already sent to "+phone_number);
        }else {
            waitForClickAbilityOf(fab_write_sms);
            driver.findElement(fab_write_sms).click();

            waitForClickAbilityOf(input_field_number);
            driver.findElement(input_field_number).click();
            driver.findElement(input_field_number).sendKeys(phone_number);
            Thread.sleep(2000);

            List<WebElement> sectionList = driver.findElements(msg_body);
            int how_many_msg = (sectionList.size() - 1);
            String first_msg_full = ReadExcel.readData(excelPath, "0", rowNo2, "13");
            String first_msg_status = ReadExcel.readData(excelPath, "0", rowNo2, "4");
            //System.out.println("1st message status: "+first_msg_status);
            //System.out.println("last message pos: "+how_many_msg);
            //System.out.println("1st message from excel: "+first_msg_full);

            int i = 0;
            for(WebElement element:sectionList){
                ///System.out.println(element.getText());
                if (element.getText().equals(first_msg_full) && i == how_many_msg){
                    System.out.println("==> Didn't get reply yet for last message: "+phone_number);
                    //System.out.println("Last msg in conversation: "+element.getText());

                    waitForClickAbilityOf(btn_back);
                    driver.findElement(btn_back).click();
                    waitForVisibilityOf(fab_write_sms);
                }else if (i == how_many_msg && element.getText() != first_msg_full && first_msg_status.equals("Sent")){
                    //System.out.println("Last msg in conversation: "+element.getText());
                    System.out.println("==> Got Reply for 1st message: "+element.getText());

                    if (element.getText().equals("No") || element.getText().equals("no") || element.getText().equals("NO") || element.getText().equals("Sold")){
                        System.out.println("Got reply as No");

                        waitForClickAbilityOf(input_field_text);
                        driver.findElement(input_field_text).click();
                        driver.findElement(input_field_text).sendKeys("Thanks");

                        waitForClickAbilityOf(btn_send);
                        driver.findElement(btn_send).click();
                        waitForVisibilityOf(btn_cam);
                        System.out.println("==> 2nd message sent to "+phone_number);

                        WriteExcel.writeData(excelPath, 0, rowNo2, 6, "Sent");
                        WriteExcel.writeData(excelPath, 0, rowNo2, 8, "Sent");
                        WriteExcel.writeData(excelPath, 0, rowNo2, 10, "Sent");
                        WriteExcel.writeData(excelPath, 0, rowNo2, 12, "Sent");

                        waitForClickAbilityOf(btn_back);
                        driver.findElement(btn_back).click();

                        Thread.sleep(1000);
                        if (driver.findElements(btn_back).size() > 0){
                            driver.findElement(btn_back).click();
                        }

                        waitForVisibilityOf(fab_write_sms);
                    }else {
                        System.out.println("==> Going to send the 2nd message!");

                        String second_msg = ReadExcel.readData(excelPath, "0", rowNo2, "5");
                        waitForClickAbilityOf(input_field_text);
                        driver.findElement(input_field_text).click();
                        driver.findElement(input_field_text).sendKeys(second_msg);

                        waitForClickAbilityOf(btn_send);
                        driver.findElement(btn_send).click();
                        waitForVisibilityOf(btn_cam);
                        System.out.println("==> 2nd message sent to "+phone_number);

                        WriteExcel.writeData(excelPath, 0, rowNo2, 6, "Sent");

                        waitForClickAbilityOf(btn_back);
                        driver.findElement(btn_back).click();
                        Thread.sleep(1000);
                        if (driver.findElements(btn_back).size() > 0){
                            driver.findElement(btn_back).click();
                        }
                        waitForVisibilityOf(fab_write_sms);
                    }

                }
                i++;
            }
            if (driver.findElements(search_result).size() <= 0){
                System.out.println("==> Didn't get reply yet for last message: "+phone_number);
                waitForClickAbilityOf(btn_back);
                driver.findElement(btn_back).click();
                Thread.sleep(1000);
                if (driver.findElements(btn_back).size() > 0){
                    driver.findElement(btn_back).click();
                }
                waitForVisibilityOf(fab_write_sms);
            }
        }
        System.out.println("*******************************************************");
    }

    @Test(priority = 4)
    public void showSep2(){
        System.out.println("==================== 2nd message part DONE ====================");
        System.out.println("==================== 3rd message part INIT ====================");
    }

    //For 3rd Msg
    int rowNo3 = 3;
    @Test(dataProvider="NumberProvider", priority = 5, enabled = true)
    public void sendMsgThree(String args) throws Exception {
        rowNo3 += 1;

        waitForVisibilityOf(search_result);

        String phone_number = ReadExcel.readData(excelPath, "0", rowNo3, "1");
        String third_msg_status = ReadExcel.readData(excelPath, "0", rowNo3, "8");

        if (third_msg_status.equals("Sent")){
            System.out.println("==> Got reply for 2nd message");
            System.out.println("==> 3rd message already sent to "+phone_number);
        }else {
            waitForClickAbilityOf(fab_write_sms);
            driver.findElement(fab_write_sms).click();

            waitForClickAbilityOf(input_field_number);
            driver.findElement(input_field_number).click();
            driver.findElement(input_field_number).sendKeys(phone_number);
            Thread.sleep(2000);

            List<WebElement> sectionList = driver.findElements(msg_body);
            int how_many_msg = (sectionList.size() - 1);
            String second_msg = ReadExcel.readData(excelPath, "0", rowNo3, "5");
            String second_msg_status = ReadExcel.readData(excelPath, "0", rowNo3, "6");

            int i = 0;
            for(WebElement element:sectionList){
                if (element.getText().equals(second_msg) && i == how_many_msg){
                    System.out.println("==> Didn't get reply yet for last message: "+phone_number);

                    waitForClickAbilityOf(btn_back);
                    driver.findElement(btn_back).click();
                    Thread.sleep(1000);
                    if (driver.findElements(btn_back).size() > 0){
                        driver.findElement(btn_back).click();
                    }
                    waitForVisibilityOf(fab_write_sms);
                }else if (i == how_many_msg && element.getText() != second_msg && second_msg_status.equals("Sent")){
                    System.out.println("==> Got Reply for 2nd message: "+element.getText());
                    System.out.println("==> Going to send the 3rd message!");

                    String third_msg = ReadExcel.readData(excelPath, "0", rowNo3, "7");
                    waitForClickAbilityOf(input_field_text);
                    driver.findElement(input_field_text).click();
                    driver.findElement(input_field_text).sendKeys(third_msg);

                    waitForClickAbilityOf(btn_send);
                    driver.findElement(btn_send).click();
                    waitForVisibilityOf(btn_cam);
                    System.out.println("==> 3rd message sent to "+phone_number);

                    WriteExcel.writeData(excelPath, 0, rowNo2, 8, "Sent");

                    waitForClickAbilityOf(btn_back);
                    driver.findElement(btn_back).click();
                    Thread.sleep(1000);
                    if (driver.findElements(btn_back).size() > 0){
                        driver.findElement(btn_back).click();
                    }
                    waitForVisibilityOf(fab_write_sms);
                }
                i++;
            }
            if (driver.findElements(search_result).size() <= 0){
                System.out.println("==> Didn't get reply yet for last message: "+phone_number);
                waitForClickAbilityOf(btn_back);
                driver.findElement(btn_back).click();
                Thread.sleep(1000);
                if (driver.findElements(btn_back).size() > 0){
                    driver.findElement(btn_back).click();
                }
                waitForVisibilityOf(fab_write_sms);
            }
        }
        System.out.println("*******************************************************");
    }

    @Test(priority = 6)
    public void showSep3(){
        System.out.println("==================== 3rd message part DONE ====================");
        System.out.println("==================== 4th message part INIT ====================");
    }

    //For 4th Msg
    int rowNo4 = 3;
    @Test(dataProvider="NumberProvider", priority = 7, enabled = true)
    public void sendMsgFour(String args) throws Exception {
        rowNo4 += 1;

        waitForVisibilityOf(search_result);

        String phone_number = ReadExcel.readData(excelPath, "0", rowNo4, "1");
        String fourth_msg_status = ReadExcel.readData(excelPath, "0", rowNo4, "10");

        if (fourth_msg_status.equals("Sent")){
            System.out.println("==> Got reply for 3rd message");
            System.out.println("==> 4th message already sent to "+phone_number);
        }else {
            waitForClickAbilityOf(fab_write_sms);
            driver.findElement(fab_write_sms).click();

            waitForClickAbilityOf(input_field_number);
            driver.findElement(input_field_number).click();
            driver.findElement(input_field_number).sendKeys(phone_number);
            Thread.sleep(2000);

            List<WebElement> sectionList = driver.findElements(msg_body);
            int how_many_msg = (sectionList.size() - 1);
            String third_msg = ReadExcel.readData(excelPath, "0", rowNo4, "7");
            String third_msg_status = ReadExcel.readData(excelPath, "0", rowNo4, "8");

            int i = 0;
            for(WebElement element:sectionList){
                if (element.getText().equals(third_msg) && i == how_many_msg){
                    System.out.println("==> Didn't get reply yet for last message: "+phone_number);

                    waitForClickAbilityOf(btn_back);
                    driver.findElement(btn_back).click();
                    Thread.sleep(1000);
                    if (driver.findElements(btn_back).size() > 0){
                        driver.findElement(btn_back).click();
                    }
                    waitForVisibilityOf(fab_write_sms);
                }else if (i == how_many_msg && element.getText() != third_msg && third_msg_status.equals("Sent")){
                    System.out.println("==> Got Reply for 3rd message: "+element.getText());
                    System.out.println("==> Going to send the 4th message!");

                    String fourth_msg = ReadExcel.readData(excelPath, "0", rowNo4, "9");
                    waitForClickAbilityOf(input_field_text);
                    driver.findElement(input_field_text).click();
                    driver.findElement(input_field_text).sendKeys(fourth_msg);

                    waitForClickAbilityOf(btn_send);
                    driver.findElement(btn_send).click();
                    waitForVisibilityOf(btn_cam);
                    System.out.println("==> 4th message sent to "+phone_number);

                    WriteExcel.writeData(excelPath, 0, rowNo4, 10, "Sent");

                    waitForClickAbilityOf(btn_back);
                    driver.findElement(btn_back).click();
                    Thread.sleep(1000);
                    if (driver.findElements(btn_back).size() > 0){
                        driver.findElement(btn_back).click();
                    }
                    waitForVisibilityOf(fab_write_sms);
                }
                i++;
            }
            if (driver.findElements(search_result).size() <= 0){
                System.out.println("==> Didn't get reply yet for last message: "+phone_number);
                waitForClickAbilityOf(btn_back);
                driver.findElement(btn_back).click();
                waitForVisibilityOf(fab_write_sms);
            }
        }
        System.out.println("*******************************************************");
    }

    @Test(priority = 8)
    public void showSep4(){
        System.out.println("==================== 4th message part DONE ====================");
        System.out.println("==================== 5th message part INIT ====================");
    }

    //For 5th Msg
    int rowNo5 = 3;
    @Test(dataProvider="NumberProvider", priority = 9, enabled = true)
    public void sendMsgFifth(String args) throws Exception {
        rowNo5 += 1;

        waitForVisibilityOf(search_result);

        String phone_number = ReadExcel.readData(excelPath, "0", rowNo5, "1");
        String fifth_msg_status = ReadExcel.readData(excelPath, "0", rowNo5, "12");

        if (fifth_msg_status.equals("Sent")){
            System.out.println("==> Got reply for 4th message");
            System.out.println("==> 5th message already sent to "+phone_number);
        }else {
            waitForClickAbilityOf(fab_write_sms);
            driver.findElement(fab_write_sms).click();

            waitForClickAbilityOf(input_field_number);
            driver.findElement(input_field_number).click();
            driver.findElement(input_field_number).sendKeys(phone_number);
            Thread.sleep(2000);

            List<WebElement> sectionList = driver.findElements(msg_body);
            int how_many_msg = (sectionList.size() - 1);
            String fourth_msg = ReadExcel.readData(excelPath, "0", rowNo5, "9");
            String fourth_msg_status = ReadExcel.readData(excelPath, "0", rowNo5, "10");

            int i = 0;
            for(WebElement element:sectionList){
                if (element.getText().equals(fourth_msg) && i == how_many_msg){
                    System.out.println("==> Didn't get reply yet for last message: "+phone_number);

                    waitForClickAbilityOf(btn_back);
                    driver.findElement(btn_back).click();
                    Thread.sleep(1000);
                    if (driver.findElements(btn_back).size() > 0){
                        driver.findElement(btn_back).click();
                    }
                    waitForVisibilityOf(fab_write_sms);
                }else if (i == how_many_msg && element.getText() != fourth_msg && fourth_msg_status.equals("Sent")){
                    System.out.println("==> Got Reply for 4th message: "+element.getText());
                    System.out.println("==> Going to send the 5th message!");

                    String fifth_msg = ReadExcel.readData(excelPath, "0", rowNo5, "11");
                    waitForClickAbilityOf(input_field_text);
                    driver.findElement(input_field_text).click();
                    driver.findElement(input_field_text).sendKeys(fifth_msg);

                    waitForClickAbilityOf(btn_send);
                    driver.findElement(btn_send).click();
                    waitForVisibilityOf(btn_cam);
                    System.out.println("==> 5th message sent to "+phone_number);

                    WriteExcel.writeData(excelPath, 0, rowNo5, 12, "Sent");

                    waitForClickAbilityOf(btn_back);
                    driver.findElement(btn_back).click();
                    Thread.sleep(1000);
                    if (driver.findElements(btn_back).size() > 0){
                        driver.findElement(btn_back).click();
                    }
                    waitForVisibilityOf(fab_write_sms);
                }
                i++;
            }
            if (driver.findElements(search_result).size() <= 0){
                System.out.println("==> Didn't get reply yet for last message: "+phone_number);
                waitForClickAbilityOf(btn_back);
                driver.findElement(btn_back).click();
                Thread.sleep(1000);
                if (driver.findElements(btn_back).size() > 0){
                    driver.findElement(btn_back).click();
                }
                waitForVisibilityOf(fab_write_sms);
            }
        }
        System.out.println("*******************************************************");
    }

    @Test(priority = 10)
    public void showSep5(){
        System.out.println("==================== 5th message part DONE ====================");
    }


    @AfterTest
    public void endBot() {
        driver.quit();
        appiumService.stop();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForClickAbilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static void swipeVertical(AppiumDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(driver).press(point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(point(anchor, endPoint)).release().perform();
    }
}
