package utils;
import java.io.File;
import java.io.FileWriter;

public class ReadImageName {
    public static void main(String[] argv) throws Exception {
        String excelPath = System.getProperty("user.dir") +"\\excel_file\\Input.xlsx";
        String imagePath = ReadExcel.readData(excelPath, "1", 1, "1");
        String path_to_folder = imagePath;
        String filePath = System.getProperty("user.dir") +"\\excel_file\\ImageName.txt";
        File my_folder = new File(path_to_folder);
        File[] array_file = my_folder.listFiles();
        for (int i = 0; i < array_file.length; i++){
            String long_file_name = array_file[i].getName();
            System.out.println(long_file_name);
            File file = new File(filePath);
            FileWriter fileWriter = new FileWriter(file,true);
            fileWriter.write(long_file_name);
            fileWriter.write("\r\n");
            fileWriter.close();
        }
    }
}