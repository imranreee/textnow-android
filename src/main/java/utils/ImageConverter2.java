package utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageConverter2 {
    public static void main(String[] args) throws IOException, IOException {
        int numberOfImages = 100;
        String excelPath = System.getProperty("user.dir") +"\\excel_file\\Input.xlsx";
        int rowNo = 5;
        String imageFolderPath = "C:\\Users\\Imran\\Desktop\\NewImages";
        String convertedImagesPath = "C:\\Users\\Imran\\Desktop\\ConvertedImages";

        for (int i = 0; i < numberOfImages; i++){
            String imageName = ReadExcel.readData(excelPath, "0", rowNo, "1");
            String temp = imageFolderPath + "\\" +imageName;
            Path absPath = Paths.get(temp);
            String imageName2 = ReadExcel.readData(excelPath, "0", rowNo, "2");
            String temp2 = convertedImagesPath + "\\" +imageName2;
            Path absPath2 = Paths.get(temp2);

            BufferedImage originalImage = ImageIO.read(absPath.toFile());

            BufferedImage newBufferedImage = new BufferedImage(
                    originalImage.getWidth(),
                    originalImage.getHeight(),
                    BufferedImage.TYPE_INT_RGB);

            newBufferedImage.createGraphics()
                    .drawImage(originalImage,
                            0,
                            0,
                            Color.WHITE,
                            null);
            ImageIO.write(newBufferedImage, "png", absPath2.toFile());
        }
    }
}
