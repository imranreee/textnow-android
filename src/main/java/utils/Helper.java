package utils;

import org.openqa.selenium.By;

import java.io.IOException;

public class Helper {
    public static void main(String args[]) throws IOException {

        //For MEmu 7.1.2
        Runtime.getRuntime().exec("adb connect localhost:21503");
        System.out.println("MEmu adb connected");

        By category = By.xpath("//android.view.ViewGroup[2]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup");
        By condition = By.xpath("//android.view.ViewGroup[2]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup");

        int xCoordinateArray[] = new int[4];
        xCoordinateArray[0] = 287;
        xCoordinateArray[1] = 481;
        xCoordinateArray[2] = 96;
        xCoordinateArray[3] = 284;

        int yCoordinateArray[] = new int[4];
        yCoordinateArray[0] = 172;
        yCoordinateArray[1] = 177;
        yCoordinateArray[2] = 385;
        yCoordinateArray[3] = 382;

        //For Android 11
        String imagesArray[] = new String[4];
        imagesArray[0] = "//android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[2]//android.widget.ImageView";
        imagesArray[1] = "//android.view.ViewGroup[3]/android.view.ViewGroup[1]/android.view.ViewGroup[2]//android.widget.ImageView";
        imagesArray[2] = "//android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView";
        imagesArray[3] = "//android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView";

        //image search result
        //new TouchAction((AndroidDriver)driver).tap(point(660, 380)).perform();

        //Android 10
        //By condition = By.xpath("//android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[3]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup");
        //By category = By.xpath("//android.view.ViewGroup[3]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup");


        //Android 9
        //android.view.ViewGroup[3]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup
        //android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[3]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup

    }
}
