package utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageConverter {

    public static void main(String args[]) throws IOException {
        int numberOfImages = 101;
        String excelPath = System.getProperty("user.dir") +"\\excel_file\\Input.xlsx";
        int rowNo = 5;
        String imageFolderPath = "C:\\Users\\Imran\\Desktop\\NewImages";
        String convertedImagesPath = "C:\\Users\\Imran\\Desktop\\ConvertedImages";

        for (int i = 0; i < numberOfImages; i++){
            rowNo += 1;
            String imageName = ReadExcel.readData(excelPath, "0", rowNo, "1");
            String imageName2 = ReadExcel.readData(excelPath, "0", rowNo, "2");
            String absPath = imageFolderPath+"\\"+imageName;
            String absPath2 = convertedImagesPath+"\\"+imageName2;
            //System.out.println(absPath);
            //System.out.println(absPath2);

            String formatName = "PNG";
            try {
                boolean result = ImageConverter.convertFormat(absPath,
                        absPath2, formatName);
                if (result) {
                    System.out.println("Image converted successfully.");
                } else {
                    System.out.println("Could not convert image.");
                }
            } catch (IOException ex) {
                System.out.println("Error during converting image.");
                ex.printStackTrace();
            }
        }
    }
    public static boolean convertFormat(String inputImagePath,
                                        String outputImagePath, String formatName) throws IOException {
        FileInputStream inputStream = new FileInputStream(inputImagePath);
        FileOutputStream outputStream = new FileOutputStream(outputImagePath);

        // reads input image from file
        BufferedImage inputImage = ImageIO.read(inputStream);

        // writes to the output image in specified format
        boolean result = ImageIO.write(inputImage, formatName, outputStream);

        // needs to close the streams
        outputStream.close();
        inputStream.close();

        return result;
    }
}
