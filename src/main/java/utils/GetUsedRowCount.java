package utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by Al Imran on 28/11/2020.
 */
public class GetUsedRowCount {

    public static int getCountUsedRow(String excelPath, String sheetNum) throws IOException {
        File src = new File(excelPath);
        FileInputStream fis = new FileInputStream(src);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet sh1 = wb.getSheetAt(Integer.parseInt(sheetNum));

        int rowCount = sh1.getPhysicalNumberOfRows();
        int numberOfAds = 0;
        for (int i = 0; i < rowCount; i++) {

            Row eachRow = sh1.getRow(i);
            for (Cell cell : eachRow) {
                if (cell != null && !cell.toString().isEmpty()) {
                    numberOfAds++;
                    break;
                }
            }
        }
        return numberOfAds;
    }
}
