package utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URL;

public class OfferUp_DeletePost_MEmu {
    WebDriver driver;
    AppiumDriverLocalService appiumService;
    String appiumServiceUrl;

    public OfferUp_DeletePost_MEmu() throws IOException {
    }

    @BeforeTest
    public void runAppiumServer() throws InterruptedException, IOException {
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();
        appiumServiceUrl = appiumService.getUrl().toString();
        appiumService.clearOutPutStreams();

        Thread.sleep(2000);
        Runtime.getRuntime().exec("adb connect localhost:21503");
    }

    @Test
    public void postAd() throws Exception {
            DesiredCapabilities dc1 = new DesiredCapabilities();
            dc1.setCapability("platformName", "android");
            dc1.setCapability("deviceName", "OnePlusOne");
            dc1.setCapability("noReset", "true");
            dc1.setCapability("autoGrantPermissions", "true");
            dc1.setCapability("appPackage", "com.offerup");
            dc1.setCapability("appActivity", "com.offerup.MainActivity");

            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc1);

            By overflow_menu = By.xpath("//android.widget.Button[3]/android.view.ViewGroup/android.view.ViewGroup");
            By pasted_item = By.xpath("//android.widget.Button[@content-desc = 'item-dashboard-screen.ellipses-button.button']");
            By sellingBtn = By.xpath("//android.widget.TextView[@content-desc = 'tab-bar-widget.tab.selling.text']");
            By archive_btn = By.xpath("//android.widget.TextView[@text = 'Archive']");

            Thread.sleep(10000);
            waitForVisibilityOf(sellingBtn);
            driver.findElement(sellingBtn).click();
            System.out.println("Clicked on selling btn");
            Thread.sleep(10000);

            Boolean is_posted_item_available;
            is_posted_item_available = driver.findElements(pasted_item).size() > 0;
            while (is_posted_item_available == true){
            driver.findElement(pasted_item).click();
            waitForVisibilityOf(overflow_menu);
            driver.findElement(overflow_menu).click();
            waitForVisibilityOf(archive_btn);
            driver.findElement(archive_btn).click();

            Thread.sleep(7000);
            is_posted_item_available = driver.findElements(pasted_item).size() > 0;
        }
        System.out.println("All old posted ad deleted");
        driver.quit();
    }

    @AfterTest
    public void endBot() throws IOException {

        appiumService.stop();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

}
